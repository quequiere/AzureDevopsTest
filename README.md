# Azure Devops
This repo is a complete test for good practice to up Azure cloud devops K8s platform


## Manual setup
Can be found in file manual.md





# Read me before start

- 99% of infrastructure is up by Terraform
- Don't forget to setup api access to Terraform
- Don't forget to setup blob storage to Terraform to save state

## Normal setup
This project up automatically 100% of infrastructure with Terraform executed from the pipeline.
But you need to provide access to terraform and key to authenticate. Perform there commands to get them

    az account list --query "[].{name:name, subscriptionId:id, tenantId:tenantId}"
    az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/MySubscriptionID"

They will be used in provider.tf (azurerm). Add them to variable pipeline.


## Save state
Terraform need to save state to remember what is up on infrastructure. To do that you need a storage on Azure.
You need to create it mannualy with theses commands

    az group create --name terraformOrga --location centralus
    az storage account create --resource-group terraformOrga --name terrastorage548784 --sku Standard_LRS --encryption-services blob
    az storage account keys list --resource-group terraformOrga --account-name terrastorage548784 --query [0].value -o tsv
    az storage container create --name terrastoragecontainer545886 --account-name terrastorage548784 --account-key KeyResultAbove

They will be used in provider.tf (terraform backend). Add them to variable pipeline.


## Connect local terminal
To connect your local terminal to k8s used

    az aks get-credentials --resource-group azure-k8stest --name k8stest

## Connect to k8s Dashboard
	az aks enable-addons --addons kube-dashboard --resource-group azure-k8stest --name k8stest
	az aks browse --resource-group azure-k8stest --name k8stest

## Restart all

- Delete all k8stest resources from groups
- Delete state in blob storage

## Pipeline schema

![](./docs/schemas-Pipeline.png)

## Kubernetes schema

![](./docs/schemas-Kubernetes.png)

## Terraform schema

![](./docs/schemas-Terraform.png)