output "registry_username" {
  value = azurerm_container_registry.acr.admin_username
}

output "registry_password" {
  value = azurerm_container_registry.acr.admin_password
}

output "registry_host" {
  value = azurerm_container_registry.acr.login_server
}

output "cluster_name" {
  value = "${azurerm_kubernetes_cluster.k8s.name}"
}

output "ressourcegroup_name" {
  value = "${azurerm_resource_group.k8s.name}"
}