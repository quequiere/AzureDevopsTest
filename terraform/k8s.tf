resource "azurerm_resource_group" "k8s" {
  name     = "azure-k8stest"
  location = "Central US"
}

resource "azurerm_container_registry" "acr" {
  name                = "k8sconainter"
  resource_group_name = azurerm_resource_group.k8s.name
  location            = azurerm_resource_group.k8s.location
  sku                 = "Standard"
  admin_enabled       = true
}


resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "k8stest"
  location            = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  dns_prefix          = "k8stest"


  default_node_pool {
    name       = "agentpool"
    node_count = 2
    vm_size    = "Standard_B2s"
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  tags = {
    Environment = "Development"
  }

  depends_on = [
    azurerm_container_registry.acr
  ]
}

resource "kubernetes_namespace" "k8ingressprod" {
  metadata {
    name = "ingress-basic"
  }
  depends_on = [
    azurerm_kubernetes_cluster.k8s
  ]
}


resource "kubernetes_namespace" "k8singresspreprod" {
  metadata {
    name = "ingress-basic-preprod"
  }

  depends_on = [
    azurerm_kubernetes_cluster.k8s
  ]
}

#Equivalent of ==> kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard
resource "kubernetes_cluster_role_binding" "k8srolebinding" {
  metadata {
    name = "kubernetes-dashboard"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "kubernetes-dashboard"
    namespace = "kube-system"
  }
}


locals {
  dockercfgdefault = {
    "${azurerm_container_registry.acr.login_server}" = {
      email    = ""
      username = azurerm_container_registry.acr.admin_username
      password = azurerm_container_registry.acr.admin_password
    }
  }

  dockercfgpreprod = {
    "${azurerm_container_registry.acr.login_server}" = {
      email    = ""
      username = azurerm_container_registry.acr.admin_username
      password = azurerm_container_registry.acr.admin_password
    }
  }
}

resource "kubernetes_secret" "regsecret-default" {
  metadata {
    name = "regsecret"
  }
  data = {
    ".dockercfg" = jsonencode(local.dockercfgdefault)
  }

  type = "kubernetes.io/dockercfg"

  depends_on = [
    kubernetes_namespace.k8ingressprod
  ]
}

resource "kubernetes_secret" "regsecret-preprod" {
  metadata {
    name      = "regsecret"
    namespace = kubernetes_namespace.k8singresspreprod.metadata[0].name
  }
  data = {
    ".dockercfg" = jsonencode(local.dockercfgpreprod)
  }

  type = "kubernetes.io/dockercfg"

  depends_on = [
    kubernetes_namespace.k8singresspreprod
  ]
}


resource "helm_release" "ingress-default" {
  name       = "nginx-ingress"
  chart      = "nginx-ingress"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  namespace  = "ingress-basic"


  set {
    name  = "controller.replicaCount"
    value = "1"
  }

 #Equivalent of 
 #--set controller.nodeSelector."beta\.kubernetes\.io/os"
  set {
    name  = "controller.nodeSelector.beta\\.kubernetes\\.io/os"
    value = "linux"
  }


  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }
  
  set {
    name  = "controller.scope.namespace"
    value = "default"
  }
  
  set {
    name  = "controller.scope.enabled"
    value = "true"
  }

  depends_on = [
    kubernetes_namespace.k8ingressprod
  ]

}


resource "helm_release" "ingress-preprod" {
  name       = "nginx-ingress-pp"
  chart      = "nginx-ingress"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  namespace  = "ingress-basic-preprod"


  set {
    name  = "controller.replicaCount"
    value = "1"
  }

 #Equivalent of 
 #--set controller.nodeSelector."beta\.kubernetes\.io/os"
  set {
    name  = "controller.nodeSelector.beta\\.kubernetes\\.io/os"
    value = "linux"
  }


  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }
  
  set {
    name  = "controller.scope.namespace"
    value = "ingress-basic-preprod"
  }
  
  set {
    name  = "controller.scope.enabled"
    value = "true"
  }

  depends_on = [
    kubernetes_namespace.k8singresspreprod
  ]

}
