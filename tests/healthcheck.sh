#!/bin/bash

checkAlive(){
	result=$(curl -s -o /dev/null -w “%{http_code}” -u toto:python -X GET $endpointUrl)
	
	if [[ “$result” == *"200"* ]] 
	then 
		echo "Website api is alive"
		exit 0

	elif [[ “$result” == *"503"* ]]
	then
		if [[ $retrycurl < 3 ]] 
		then 
			echo "Website is maybe not up yet, retry $retrycurl result $result"
			((retrycurl++))
			sleep 30
			checkAlive
		else
			echo "Failed to get website $endpointUrl after $retrycurl try"
			exit 1
		fi
		
	else 
		echo "Fail to check api ==> $result" ; 
		exit 1 
	fi

}

endpointUrl="http://${ppexternalip}/pozos/api/v1.0/get_student_ages"
echo "Testing endpoint $endpointUrl"
retrycurl=0
checkAlive