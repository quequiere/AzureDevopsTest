#!/bin/bash

getExternalIp(){
	externalip=$(kubectl get services nginx-ingress-controller --namespace ingress-basic -o=jsonpath={.status.loadBalancer.ingress[0].ip})
	
	if [ -z "$externalip" ]
	then
		  echo "Failed to get external ip, retry $retry, next test in 60 seconds"
		  ((retry++))
		  
			if [[ $retry > 3 ]]
			then
				echo "Failed to get external ip after $retry retry"
				exit 1
			fi
			
		  sleep 60
		  getExternalIp
	else
		  echo "Success find external ip $externalip"
		  echo "##vso[task.setvariable variable=externalip]$externalip"
	fi

}

az login -u ${client_id} -p ${client_secret}
az aks get-credentials --resource-group azure-k8stest --name k8stest
echo "Getting external ip..."
retry=0
getExternalIp
