## Build localy:

    docker build -t quequiere.azurecr.io/simple_api:latest ./simple_api
    docker build -t quequiere.azurecr.io/simple_website:latest ./website
    docker-compose up

## Push to registry

    docker login quequiere.azurecr.io
    docker push quequiere.azurecr.io/simple_api:latest
    docker push quequiere.azurecr.io/simple_website:latest 


## K8s management
#### Access to dashboard & add auth:

    kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard
    az aks browse --resource-group azure-testk8s --name k8stest
#### Initiliaze client kubectl:

    az aks get-credentials --resource-group myResourceGroup --name myAKSCluster


#### Connect K8s to private registry 

> https://kubernetes.io/fr/docs/tasks/configure-pod-container/pull-image-private-registry

    kubectl create secret docker-registry regcred --docker-server=quequiere.azurecr.io --docker-username=quequiere --docker-password=MyPassword
	
	kubectl create secret docker-registry regcred --docker-server=quequiere.azurecr.io --docker-username=quequiere --docker-password=MyPassword --namespace ingress-basic-preprod

## K8s Ingress

> https://docs.microsoft.com/fr-fr/azure/aks/ingress-basic

    helm repo add stable https://kubernetes-charts.storage.googleapis.com
	

#### Install ingress controller:

    kubectl create namespace ingress-basic
    helm install nginx-ingress stable/nginx-ingress --namespace ingress-basic --set controller.replicaCount=2 --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux --set controller.service.externalTrafficPolicy=Local
	
And second one for preprod
	
    kubectl create namespace ingress-basic-preprod
    helm install nginx-ingress-pp stable/nginx-ingress --namespace ingress-basic-preprod --set controller.replicaCount=2 --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux --set controller.service.externalTrafficPolicy=Local --set controller.scope.namespace=ingress-basic-preprod --set controller.scope.enabled=true

#### Verify installation:

    kubectl get service -l app=nginx-ingress --namespace ingress-basic

#### Uninstall:

    kubectl delete namespace ingress-basic
    kubectl delete namespace ingress-basic-preprod


## K8s repository docker

#### Configure cli to be bind on K8S:

    az aks get-credentials --resource-group testdevops --name quequierecluster


## K8s deploy

#### To Deploy

    kubectl apply -f kubernetes-Deployments.yaml

#### To delete

    kubectl delete -f kubernetes-Deployments.yaml

#### To visualize:

    kubectl get service azure-simpleapi-service --watch
    kubectl get service azure-simplewebsite --watch


## Access apps with ingress gateway

> https://docs.microsoft.com/fr-fr/azure/aks/ingress-basic

#### To get the gateway:

    kubectl get service nginx-ingress-controller --namespace ingress-basic

http://ip/pozos/api/v1.0/get_student_ages  (toto:python)
http://ip:80
